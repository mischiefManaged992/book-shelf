import React from "react";
import classes from "./link.css";

const link = () => {
    return(
        <a href="#" className={classes.link}>Link</a> 
    );
}

export default link;